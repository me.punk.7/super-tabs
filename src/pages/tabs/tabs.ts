import {AfterViewInit, Component, ViewChild } from '@angular/core';
import { IonicPage, Events, Tabs, NavController } from 'ionic-angular';

//import {SuperTabsComponent} from 'ionic2-super-tabs';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { NewpagePage } from '../newpage/newpage'
import {SwipeTabDirective} from '../../directives/swipe-tab.directive';
//import { SuperTab } from '@ionic-super-tabs/angular';
import { SuperTabs } from '@ionic-super-tabs/angular';
import { SuperTabsConfig} from '@ionic-super-tabs/core';
//import { SuperTabsComponent } from '@ionic-super-tabs/core';
@IonicPage()
@Component({
templateUrl: 'tabs.html'
})
export class TabsPage implements AfterViewInit {
@ViewChild(SuperTabs) superTabs: SuperTabs;
@ViewChild(SwipeTabDirective) swipeTabDirective: SwipeTabDirective;
@ViewChild('myTabs') tabRef: Tabs;
public isCollapsed = false;
pages: Array<{pageName: any, title: string, icon: string, id: string}>;

selectedTab = 0;
constructor(public events: Events,public navCtrl: NavController) {
this.events.subscribe('closeTab', ()=> this.removePage());
events.subscribe('addPage', (data) => {
this.pages.push({pageName: NewpagePage, title: 'New Page', icon: 'call', id: 'newpage4Tab'});
var index = this.pages.findIndex(item => item.id === 'newpage4Tab');
this.selectedTab = index;
this.superTabs.selectTab(index);
});
}

opts = {
icon: true,
label: true,
toolbarPos: 'top',
scrollable: true,
};

config: SuperTabsConfig = {
debug: true,
allowElementScroll: false,
};

ngOnInit() {
this.pages=[{ pageName: HomePage, title: 'Home', icon: 'flame', id: 'HomeTab'},
{ pageName: AboutPage, title: 'About', icon: 'flame', id: 'AboutTab'},
{ pageName: ContactPage, title: 'Contact', icon: 'flame', id: 'ContactTab'}];
}

removePage(){
let index = this.selectedTab;
if (index == 0) {
this.pages.shift();
}else {
this.selectedTab--;
this.pages.splice(index, 1);
}
}

swipeEvent(e) {
if(e.direction == '2'){
var rightLen=this.selectedTab+1;
if(rightLen == this.pages.length) return;
this.selectedTab++
this.superTabs.selectTab(this.selectedTab);
}
else if(e.direction == '4'){
if(this.selectedTab==0) return;
this.selectedTab--
this.superTabs.selectTab(this.selectedTab);
}
}


onTabChange(ev: any) {
console.log('Tab change fired', ev);
this.selectedTab  = ev.detail.index;
}

onActiveTabIndexChange(ev: any){
console.log(ev);
}

trackByFn(index, item) {
return index;
}
ngAfterViewInit() {
this.superTabs.selectTab(0);
}



}
