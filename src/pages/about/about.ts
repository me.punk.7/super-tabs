import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController, private iab: InAppBrowser) {
    this.openTab();
  }


  openTab(){
 const browser = this.iab.create('https://www.sagoonlite.com/', '_blank','location=yes');
 // var x =  window.open('https://www.sagoonlite.com/', '_blank', 'location=no');
browser.show();


    //browser.executeScript(...);
    
    //browser.insertCSS(...);
    //browser.on('loadstop').subscribe(event => {
      // browser.insertCSS({ code: "body{color: red;" });
    //});
    
    //browser.close();
  }



}
